<!DOCTYPE html>
<html>
<head>
    <title> Gifts </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/bootstrap-4.3.1-dist/js/bootstrap.min.js">
    <link rel="stylesheet" href="../assets/css/gift_page_style.css">
</head>

<body> 
 
<div class="container">
    <div class="row">   
        <img id="girl" class="img-fluid" src="../images/gifts/girl.jpg">
        <a href="../public"><img class="logo" src="../images/logo.png"></a>
        <a id="covid" href=""> COVID-19 </a>
        <a id="gifts" href="../public/gifts">Gifts</a>
        <a id="faq" href="../public/faq">FAQ</a>
        <a id="english" href="">English</a>
        <a id="login" href="">Login</a>
       <a id="signup" href="">Sign up</a>  
       <h2 id="h41"> Choose together </h2>
        <a id="btn1" role="button"  href="#" class="btn" > Order </a>

    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12 col-sm-4">
           <a href=""> <img class="img-fluid" src="../images/gifts/pic1.png"> </a>
        </div>

        <div class="col-12 col-sm-4">
           <a href=""> <img class="img-fluid" src="../images/gifts/pic2.png"> </a>
        </div>

        <div class="col-12 col-sm-4">
           <a href=""> <img class="img-fluid" src="../images/gifts/pic3.png"> </a>
        </div>

        
    </div>
    <hr /> 
</div>


<div class="container">
    <div class="row">
        <div class="col-12 col-sm-4">
           <a href=""> <img class="img-fluid" src="../images/gifts/pic4.png"> </a>
        </div>

        <div class="col-12 col-sm-4">
           <a href=""> <img class="img-fluid" src="../images/gifts/pic5.png"> </a>
        </div>

        <div class="col-12 col-sm-4">
           <a href=""> <img class="img-fluid" src="../images/gifts/pic6.png"> </a>
        </div>

    </div>
    <div class="container">
        <hr />
        <a id="hellomarket" href=""> <p id="hellomarket"> HellOOmarket.com </p></a>
        <p id="text1"> You offer, They chose, We deliver </p>
    </div>
</div>
<div class="container">
    <div class="text-right">
        <p style="font-family: Helvetica, sans-serif; color:rgb(180, 180, 180); font-size: 13px;"> HellOOexpress &copy; 2020. All Rights Reserved. </p>
    </div>
</div>




</body>
</html> 
</body>
</html>