<!DOCTYPE html>
<html>
<head>
	<title> hellOOexpress </title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" href="../assets/css/styles.css">

</head>
<body>
<!-- slide 1-->
<div class="container offset-sm-1">
		<img class="girl1" src="../images/girl1.jpg" width="1000" height="500">
		<div class="row" >
			
			<a id="covid" href="">COVID-19</a>
			<a id="gifts" href="../public/gift">Gifts</a>
			<a id="faq" href="../public/faq">FAQ</a>
			<a id="english" href="">English</a>
			<a id="login" href="">Login</a>
			<a id="signup" href="">Sign up</a>
		</div>
		
		<img  class="flag" src="../images/flag.png" >
		<img class="line" src="../images/line.png" >
		<img class="dot1" src="../images/dot.png" >
		<img class="dot2" src="../images/dot.png" >
		<img class="dot3" src="../images/dot.png" >
		<h5 class="h51"> You offer </h5>
		<h5 class="h52"> They Choose </h5>
		<h5 class="h53"> We deliver </h5>
		<img class="pic11" src="../images/gift.png" >
		<img class="pic12" src="../images/roaster.png" >
		<img class="pic13" src="../images/delivery.png" >
		<a role="button"  href="#" class="btn" > Get Started </a>
		<img class="pic14" src="../images/visa.png" >
		<img class="pic15" src="../images/paypal.png" >
		<!--<img class="logo" src="../images/nav.png">-->

</div>

<!-- endof slide 1--> 
<!-- slide 2 -->
<div class="container">
	<div class="row">
		<img class="img-fluid" src="../images/upperbar.png" width="1000" height="70">
		<div class="col-sm-4">
			<a href=""><img src="../images/pic1.png" class="img-fluid"></a>
		</div>

		<div class="col-sm-4">
		<a href=""><img src="../images/pic2.png" class="img-fluid" ></a>
		</div>

		<div class="col-sm-4">
		<a href=""><img src="../images/pic3.png" class="img-fluid" ></a>
		</div>
		<p id="p21"> You offer, They choose, We deliver </p>
		<img src="../images/upperbar.png" width="1000" height="70">
	</div>
</div>
<!-- endof slide 2-->

<!-- slide 3 -->

<div class="container">
	<div class="row ">
		<div class="col-sm-4">
			<h5> You order </h5>
		</div>

		<div class="col-sm-4">
			<h5> They choose </h5>
		</div>

		<div class="col-sm-4">
			<h5> We deliver </h5>
		</div>
	</div>

	<div class="row">
		<img id="line3" src="../images/line.png" >
		<img id="dot31" src="../images/dot.png" >
		<img id="dot32" src="../images/dot.png" >
		<img id="dot33" src="../images/dot.png" >
	</div>
   
	<div class="row ">
		<div class="col-sm-4">
			<img src="../images/gift.png" width="150" height="150">
		</div>
		<div class="col-sm-4">
			<img src="../images/roaster.png" width="150" height="150">
		</div>
		<div class="col-sm-4">
			<img src="../images/delivery.png" width="200" height="200">
		</div>
	</div>

	<div class="container">
		<div class="row 1">
			<div class="col-sm-4">
				<h5> Order your Express Gift Card </h5>
				<ul class="tickmark">
					<li class="ticked"> Enter the email or phone number of the beneficiary &amp; your personal message</li>
					<li class="ticked"> Choose amount </li>
					<li class="ticked"> Pay </li>
				</ul>
				
				
			</div>

			<div class="col-sm-4">
				<h5> Beneficiary login on HellOOmarket.com or call 8420 </h5>
				<ul class="tickmark">
					<li class="ticked"> Choose his favorite items</li>
					<li class="ticked"> Pay with his gift code </li>
				</ul>
				
				
			</div>

			<div class="col-sm-4">
				<h5> Order is delivered with Best COVID-19 practice </h5>
				<p> Delivery time </p>
				<ul class="tickmark">
					<li class="ticked"> Addis Ababa 1 working day</li>
					<li class="ticked"> Out of Addis Ababa 4 working days </li>
				</ul>
				
				
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3  ">
				<p> The beneficiary will receive his gift code via email or SMS. </p>
			</div>
			<div class="col-sm-3 offset-sm-1">
				<p> The beneficiary will receive confirmation of delivery date </p>
			</div>
			<div class="col-sm-3 offset-sm-1">
				<p> Beneficiary can refuse his order or his gift card will be reloaded </p>
			</div>
		</div>



		<div class="row">
			<div class="col-sm-3  ">
				<button> Learn more </button>
			</div>
			<div class="col-sm-3 offset-sm-1">
				<button> Learn more </button>
			</div>
			<div class="col-sm-3  offset-sm-1">
				<button> Learn more </button>
			</div>
		</div>
	</div>
</div>

<!-- endof slide 3 -->

<!-- slide 4 -->
<div class="container">
	<div class="row">
	<img src="../images/upperbar.png" width="1000" height="70">
		<div class="col-sm-4">
			<h5 style=" padding-top: 10px;font-family: Helvetica, sans-serif; color:rgb(206, 202, 202); font-size: 12px;"> SAFE AND SECURE </h5>
			<p> Your money is in safe hands </p>
			<hr class=" accent-3 mb-4 mt-0 d-inline-block mx-auto" style="color: green; width: 40px;" />
			<p style="font-family: Helvetica, sans-serif; color:rgb(206, 202, 202); font-size: 15px;"> Our industry-leading technology protects your money and<br /> guarantees it arrives safely every time.</p>

		</div>
		<div class="col-sm-8">
			<img  src="../images/visa.png" >
			<img  src="../images/paypal.png" >
		</div>

		<img  src="../images/upperbar.png" width="1000" height="70">
	</div>
</div>
<!-- endof slide 4-->


<!-- slide 5 -->

<h5 class="why"> Why HellOOexpress? </h5>
<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<h5> Fast </h5>
			<p> 100% of our transactions are live </p>
		</div>

		<div class="col-sm-3">
			<h5>Easy</h5>
			<p> Simple click, order your online gift card from $25, instantly received by beneficiary</p>
		</div>

		<div class="col-sm-3">
			<h5> Affordable </h5>
			<p> Our fair pricing is in conformity with the Ethiopian market. </p>
		</div>

		<div class="col-sm-3">
			<h5> Full Catalog </h5>
			<p> Our catalog offers an extended choice of traditional and non-traditional items. As diverse as clothes,sanitary products, toys, food and beverages, etc. </p>

		</div>

		
	</div>

	<div class="row">
		<div class="col-sm-3">
			<img src="../images/train.png" width="200" height="200">
		</div>

		<div class="col-sm-3">
			<img src="../images/coffee.png" width="150" height="150">
		</div>

		<div class="col-sm-3">
			<img src="../images/histogram.png" width="150" height="150">
		</div>

		<div class="col-sm-3">
			<img src="../images/book.png" width="150" height="150">
		</div>
		
	</div>
</div> 

<!-- endof slide 5-->

<!-- slide 6-->
<div class="container">
	<div class="row">
		<img  class="img-fluid" class="girlpic" src="../images/girl.jpg" width="1100" height="500">
		<img   class="phonepic" src="../images/phone.png" width="500" height="250">
		<img   class="playstore" src="../images/playstore.png" width="100" height="100">
	</div>
</div>

<!-- endof slide 6-->
<footer>
	<div class="container">
		<img   src="../images/upperbar.png" width="1000" height="70">
		<div class="row">
			<div class="col-6 col-sm-2 offset-sm-1">
				<h5> Hell<span style="white-space: nowrap; color: rgb(128, 0, 128);">OO</span>express </h5>
				<ul id="footer1" class="list-group">
					<li class="list-group-item border-0"> <a id="footer1" href="#"> About us </a> </li> 
					<li class="list-group-item border-0">  <a id="footer1" href="#">Our mission </a> </li> 
					<li class="list-group-item border-0"> <a id="footer1" href="#"> Our story </a> </li> 
					<li class="list-group-item border-0"> <a id="footer1" href="#">"Hello" Brand </a> </li> 
					<li class="list-group-item border-0">  <a id="footer1" href="#">Make a difference </a> </li> 
					<li class="list-group-item border-0">  <a id="footer1" href="#">News  </a></li> 
					<li class="list-group-item border-0">  <a id="footer1" href="#">Join us </a> </li>
				</ul>
				
				<hr cass="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 90px;" />
			</div>

			<div class="col-6 col-sm-2">
				<h5> Partnership </h5>
				<ul  class="list-group">
					<li class="list-group-item border-0 ">  <a id="footer1" href="#">  Partners </a> </li> 
					<li class="list-group-item border-0 ">  <a id="footer1" href="#"> Affiliates </a> </li> 
					<li class="list-group-item border-0 "> <a id="footer1" href="#"> Events </a> </li> 
				</ul>
				<hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 90px;" />
			</div>

            <div class="row">

                <div class="col-6 col-sm-3">
                    <h5> Contact us </h5>
                    <ul  class="list-group">
                        <li class="list-group-item border-0 "> <a id="footer1" href="#">  Call us </a> </li> 
                        <li class="list-group-item border-0 "> <a id="footer1" href="#">  Email us </a> </li> 
                        <li class="list-group-item border-0 "> <a id="footer1" href="#">  Our offices </a> </li> 
                    </ul>
                    <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 90px;" />
                </div>
                
               
                
                <div class="col-6 col-sm-5 offset-sm-1">
                        <h5> Legal </h5>
                        <ul  class="list-group">
                            <li class="list-group-item border-0 "> <a id="footer1" href="#"> Terms and conditions </a> </li> 
                            <li class="list-group-item border-0 "> <a id="footer1" href="#"> Privacy policy </a> </li> 
                        </ul>
                        <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 90px;" />
                        
                </div>

                <div class="col-6 col-sm-3">
                        <h5> Sign up </h5>
                        <ul  class="list-group">
                            <li class="list-group-item border-0"> <a id="signupFooter" href=""> Sign up </a> </li> 
                        </ul>
                        
                    
                </div>
                <div class="col-sm-3 offset-sm-5">
                            <h5> Follow us </h5>
                            <i style="padding-right : 2px;" class="fa fa-facebook-f"></i>
                            <i class="fa fa-twitter"></i>
                            <i class="fa fa-instagram"></i>
                            <i class="fa fa-linkedin"></i>
                    
                        </div>
            </div>

				
		</div>
			
	

					
				
	
</div>
</footer>

</body>
</html>