<!DOCTYPE html>
<html>
<head>
    <title> FAQ </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/bootstrap-4.3.1-dist/js/bootstrap.min.js">
    <link rel="stylesheet" href="../assets/css/faq_page_style.css">
</head>

<body> 
 
<div class="container">
    <div class="row">   
        <img id="boy" class="img-fluid" src="../images/faq/boy.jpg">
        <a href="../public"><img class="logo" src="../images/logo.png"></a>
        <a id="covid" href=""> COVID-19 </a>
        <a id="gifts" href="../public/gift">Gifts</a>
        <a id="faq" href="../public/faq">FAQ</a>
        <a id="english" href="">English</a>
        <a id="login" href="">Login</a>
       <a id="signup" href="">Sign up</a>  
       <a id="btn1" role="button"  href="#" class="btn" > Order </a>

    </div>
</div>


<div class="container">
    <div class="row row-content">
        <div class="col-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" role="tab" id="peterhead">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-target="#peter">Can an Express gift card be used more than once?
                            </a>
                        </h5>
                    </div>
                    <div role="tabpanel" class="collapse show" id="peter" data-parent="#accordion">
                        <div class="card-body">
                            <p class="d-none d-sm-block">Yes, provided there is still a balance remaining on the card. <p>
                        </div>
                    
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" role="tab" id="peterhead">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-target="#peter">Can an Express gift cards be reloded?
                            </a>
                        </h5>
                    </div>
                    <div role="tabpanel" class="collapse show" id="peter" data-parent="#accordion">
                        <div class="card-body">
                            <p class="d-none d-sm-block">No, you can't reload your Express gift card. <p>
                        </div>
                    
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" role="tab" id="peterhead">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-target="#peter">Can an Express gift card be issued with an initial value?
                            </a>
                        </h5>
                    </div>
                    <div role="tabpanel" class="collapse show" id="peter" data-parent="#accordion">
                        <div class="card-body">
                            <p class="d-none d-sm-block">Express gift cards have a maxiumu value 0f 25 USD(or equivalent in your currency) when issued. <p>
                        </div>
                    
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" role="tab" id="peterhead">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-target="#peter">Can more than one Express gift card be used towards a purchase?
                            </a>
                        </h5>
                    </div>
                    <div role="tabpanel" class="collapse show" id="peter" data-parent="#accordion">
                        <div class="card-body">
                            <p class="d-none d-sm-block">Yes, a customer can redeem another Express gift card during checkout. <p>
                        </div>
                    
                    </div>
                </div>


                <div class="card">
                    <div class="card-header" role="tab" id="peterhead">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-target="#peter">Can I issue an Express gift card without a customer purchasing it?
                            </a>
                        </h5>
                    </div>
                    <div role="tabpanel" class="collapse show" id="peter" data-parent="#accordion">
                        <div class="card-body">
                            <p class="d-none d-sm-block">Yes, you can. <p>
                        </div>
                    
                    </div>
                </div>

                
            </div>
        </div>
    </div>
</div>

<div class="container">
	<div class="row">
	<img src="../images/upperbar.png" width="1000" height="70">
		<div class="col-sm-4">
			<h5 style=" padding-top: 10px;font-family: Helvetica, sans-serif; color:rgb(206, 202, 202); font-size: 12px;"> SAFE AND SECURE </h5>
			<p> Your money is in safe hands </p>
			<hr class=" accent-3 mb-4 mt-0 d-inline-block mx-auto" style="color: green; width: 40px;" />
			<p style="font-family: Helvetica, sans-serif; color:rgb(206, 202, 202); font-size: 15px;"> Our industry-leading technology protects your money and<br /> guarantees it arrives safely every time.</p>

		</div>
		<div class="col-sm-8">
			<img  src="../images/visa.png" >
			<img  src="../images/paypal.png" >
		</div>

		<img  src="../images/upperbar.png" width="1000" height="70">
	</div>
</div>

<div class="container">
	<div class="row">
		<img  class="img-fluid" class="girlpic" src="../images/girl.jpg" width="1100" height="500">
		<img   class="phonepic" src="../images/phone.png" width="500" height="250">
		<img   class="playstore" src="../images/playstore.png" width="100" height="100">
	</div>
</div>

<footer>
	<div class="container">
		<img   src="../images/upperbar.png" width="1000" height="70">
		<div class="row">
			<div class="col-6 col-sm-2 offset-sm-1">
				<h5> Hell<span style="white-space: nowrap; color: rgb(128, 0, 128);">OO</span>express </h5>
				<ul id="footer1" class="list-group">
					<li class="list-group-item border-0"> <a id="footer1" href="#"> About us </a> </li> 
					<li class="list-group-item border-0">  <a id="footer1" href="#">Our mission </a> </li> 
					<li class="list-group-item border-0"> <a id="footer1" href="#"> Our story </a> </li> 
					<li class="list-group-item border-0"> <a id="footer1" href="#">"Hello" Brand </a> </li> 
					<li class="list-group-item border-0">  <a id="footer1" href="#">Make a difference </a> </li> 
					<li class="list-group-item border-0">  <a id="footer1" href="#">News  </a></li> 
					<li class="list-group-item border-0">  <a id="footer1" href="#">Join us </a> </li>
				</ul>
				
				<hr cass="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 90px;" />
			</div>

			<div class="col-6 col-sm-2">
				<h5> Partnership </h5>
				<ul  class="list-group">
					<li class="list-group-item border-0 ">  <a id="footer1" href="#">  Partners </a> </li> 
					<li class="list-group-item border-0 ">  <a id="footer1" href="#"> Affiliates </a> </li> 
					<li class="list-group-item border-0 "> <a id="footer1" href="#"> Events </a> </li> 
				</ul>
				<hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 90px;" />
			</div>

            <div class="row">

                <div class="col-6 col-sm-3">
                    <h5> Contact us </h5>
                    <ul  class="list-group">
                        <li class="list-group-item border-0 "> <a id="footer1" href="#">  Call us </a> </li> 
                        <li class="list-group-item border-0 "> <a id="footer1" href="#">  Email us </a> </li> 
                        <li class="list-group-item border-0 "> <a id="footer1" href="#">  Our offices </a> </li> 
                    </ul>
                    <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 90px;" />
                </div>
                
               
                
                <div class="col-6 col-sm-5 offset-sm-1">
                        <h5> Legal </h5>
                        <ul  class="list-group">
                            <li class="list-group-item border-0 "> <a id="footer1" href="#"> Terms and conditions </a> </li> 
                            <li class="list-group-item border-0 "> <a id="footer1" href="#"> Privacy policy </a> </li> 
                        </ul>
                        <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 90px;" />
                        
                </div>

                <div class="col-6 col-sm-3">
                        <h5> Sign up </h5>
                        <ul  class="list-group">
                            <li class="list-group-item border-0"> <a id="signupFooter" href=""> Sign up </a> </li> 
                        </ul>
                        
                    
                </div>
                <div class="col-sm-3 offset-sm-5">
                            <h5> Follow us </h5>
                            <i  class="fa fa-facebook-f"></i>
                            <i class="fa fa-twitter"></i>
                            <i class="fa fa-instagram"></i>
                            <i class="fa fa-linkedin"></i>
                    
                        </div>
            </div>

				
		</div>
			
	

					
				
	
</div>
</footer>
</body>
</html>