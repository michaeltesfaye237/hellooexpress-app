<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		
		return view('welcome_message');
	}

	//--------------------------------------------------------------------

	public function gift(){
		return view('gift_page');
	}

	public function faq(){
		return view('faq_page');
	}

	public function test(){
		return view('gift_page');
	}

}
